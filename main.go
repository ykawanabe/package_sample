package main

import "./package_sample"
import "log"

func main() {
	package_sample.LogOriginal()
	log.Print("This one is from main package")
}
